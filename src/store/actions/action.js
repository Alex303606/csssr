import { ADD_TODO } from "./actionType";

export const addTodo = todo => ({
	type: ADD_TODO,
	payload: todo,
});