import { ADD_TODO } from "../actions/actionType";
import nanoid from "nanoid";

const initialState = {
	todos: []
};

const reducer = (state = initialState, action) => {
	switch (action.type) {
		case ADD_TODO:
			return {...state, todos: [...state.todos, {text: action.payload, id: nanoid()}]};
		default:
			return state
	}
};

export default reducer;