import React, { Component } from 'react';
import { connect } from 'react-redux';
import { addTodo } from "../../store/actions/action";
import './ToDo.css';

class ToDo extends Component {
	
	state = {
		todoText: ''
	};
	
	updateText = (e) => {
		this.setState({todoText: e.target.value});
	};
	
	addTodo = () => {
		this.props.addTodo(this.state.todoText);
		this.setState({todoText: ''});
	};
	
	render() {
		return (
			<div className="todo-container">
				<h1>{this.props.title || 'Без названия'}</h1>
				<div className="form">
					<input className="form_input"
					       value={this.state.todoText}
					       placeholder="Название задачи"
					       onChange={this.updateText}
					/>
					<button className="form_button" onClick={this.addTodo}>Добавить</button>
				</div>
				{
					this.props.todos.length !== 0 &&
					<ul className="todo-list">
						{this.props.todos.map(todo =>
							<li key={todo.id} className="todo-list__item">{todo.text}</li>
						)}
					</ul>
				}
			</div>
		)
	}
}

const mapStateToProps = state => ({
	todos: state.todos
});

const mapDispatchToProps = dispatch => ({
	addTodo: text => dispatch(addTodo(text)),
});

export default connect(mapStateToProps, mapDispatchToProps)(ToDo);
